tool
extends EditorPlugin

"""
Data Folder Deleter
Copyright (C) 2020 Kutay Güler

Data Folder Deleter is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Data Folder Deleter is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

var _button: Button
var project_name: String = ProjectSettings.get("application/config/name") 
var username: String


func _enter_tree():
	_button = preload("res://addons/data_folder_deleter/button.tscn").instance()
	add_control_to_bottom_panel(_button, "Delete")
	_button.connect("pressed", self, "_on_button_pressed")


func _exit_tree():
	remove_control_from_bottom_panel(_button)
	_button.queue_free()


func delete_files_in_directory(path: String):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)

	dir.list_dir_end()
	var file_path: String
	
	for i in files.size():
		file_path = files[i]
		dir.remove(file_path)


func _on_button_pressed():
	var dir = Directory.new()
	delete_files_in_directory("user://")
	print("Project Data Folder has been cleared.")
